package com.waroengweb.pdamtirtasakti.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseTagihan {
    private int recordsTotal;
    @SerializedName("data") private List<Tagihan> tagihan = new ArrayList<>();



    public List<Tagihan> getTagihan() {
        return tagihan;
    }

    public void setTagihan(List<Tagihan> tagihan) {
        this.tagihan = tagihan;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }
}
