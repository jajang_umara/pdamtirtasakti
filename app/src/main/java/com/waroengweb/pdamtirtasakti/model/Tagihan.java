package com.waroengweb.pdamtirtasakti.model;

import com.google.gson.annotations.SerializedName;

public class Tagihan {

    @SerializedName("rek_bln") private String bulan;
    @SerializedName("rek_pakai") private String total;
    @SerializedName("rek_total") private String bayar;
    @SerializedName("byr_tgl") private String tanggal;
    @SerializedName("byr_loket") private String loket;
    @SerializedName("pel_no") private String number;
    @SerializedName("pel_nama") private String nama;
    @SerializedName("pel_alamat") private String alamat;
    @SerializedName("gol_kel") private String golongan;

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getBayar() {
        return bayar;
    }

    public void setBayar(String bayar) {
        this.bayar = bayar;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLoket() {
        return loket;
    }

    public void setLoket(String loket) {
        this.loket = loket;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
