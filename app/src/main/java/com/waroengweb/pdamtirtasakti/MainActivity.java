package com.waroengweb.pdamtirtasakti;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.waroengweb.pdamtirtasakti.adapter.TagihanAdapter;
import com.waroengweb.pdamtirtasakti.app.AppController;
import com.waroengweb.pdamtirtasakti.config.Config;
import com.waroengweb.pdamtirtasakti.helper.EndLessOnScrollListener;
import com.waroengweb.pdamtirtasakti.model.ResponseTagihan;
import com.waroengweb.pdamtirtasakti.model.Tagihan;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog pd;
    private TagihanAdapter tAdapter;
    private List<Tagihan> tagihanList = new ArrayList<>();
    private String searchString;
    private int page = 0;
    private int draw = 1;
    private EndLessOnScrollListener endLessOnScrollListener;

    @BindViews({R.id.pel_no,R.id.pel_nama,R.id.pel_alamat,R.id.gol_kel})
    List<TextView> textViews;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.search_view) SearchView searchV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("CEK TAGIHAN");
        getSupportActionBar().setSubtitle("PDAM Tirta Sakti Kerinci");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher_new);

        ButterKnife.bind(this);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");

        endLessOnScrollListener = new EndLessOnScrollListener() {
            @Override
            public void onLoadMore() {
                getTagihan(searchString);
            }
        };

        tAdapter = new TagihanAdapter(tagihanList);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(tAdapter);
        recyclerView.addOnScrollListener(endLessOnScrollListener);

        searchV.setQueryHint("Masukan 6 Digit Nomor Sambungan");
        //searchV.setSubmitButtonEnabled(true);
        TextView searchText = (TextView)  searchV.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);


        searchV.onActionViewExpanded();
        searchV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchString = s;
                page = 0;
                endLessOnScrollListener.resetState();

                getTagihan(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

    }



    private void getTagihan(final String nopel)
    {
        //Toast.makeText(getApplicationContext(), String.valueOf(page), Toast.LENGTH_SHORT).show();
        pd.show();
        VolleyLog.DEBUG = true;
        StringRequest strReq = new StringRequest(Request.Method.POST, Config.BASE_URL + "rekening/view_rinci.php?data=" +nopel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                AppController.getInstance().getRequestQueue().getCache().clear();
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                if (page == 0){
                    tagihanList.clear();
                    recyclerView.getLayoutManager().scrollToPosition(0);
                }
                ResponseTagihan responseTagihan = new Gson().fromJson(response, ResponseTagihan.class);
                if (responseTagihan.getTagihan().size() > 0) {

                    Tagihan tagihan = responseTagihan.getTagihan().get(0);
                    textViews.get(0).setText(tagihan.getNumber());
                    textViews.get(1).setText(tagihan.getNama());
                    textViews.get(2).setText(tagihan.getAlamat());
                    textViews.get(3).setText(tagihan.getGolongan());

                    for (Tagihan tagihan1 : responseTagihan.getTagihan()){
                        tagihanList.add(tagihan1);
                    }

                    tAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getApplicationContext(), "Data Tidak ditemukan", Toast.LENGTH_SHORT).show();
                }

                draw++;
                page = page+10;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("start",String.valueOf(page));
                params.put("length","10");
                params.put("draw",String.valueOf(draw));
                params.put("filter[0][name]","pel_no");
                params.put("filter[0][value]",nopel);
                params.put("order[0][column]","0");
                params.put("order[0][dir]","asc");

                return params;
            }
			


        };

        AppController.getInstance().addToRequestQueue(strReq, "String Tag");
    }

    @OnClick(R.id.cek_btn)
    void cekClick(){
        searchV.setQuery(searchV.getQuery(),true);
    }
}
