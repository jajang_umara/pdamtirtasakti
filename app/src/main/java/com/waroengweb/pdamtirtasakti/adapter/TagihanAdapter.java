package com.waroengweb.pdamtirtasakti.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.waroengweb.pdamtirtasakti.R;
import com.waroengweb.pdamtirtasakti.model.Tagihan;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class TagihanAdapter extends RecyclerView.Adapter<TagihanAdapter.MyViewHolder> {

    private List<Tagihan> listTagihan;
    private boolean loading = false;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.bulan,R.id.pakai,R.id.total,R.id.tanggal,R.id.loket})
        List<TextView> textViews;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public TagihanAdapter(List<Tagihan> listTagihan) {
        this.listTagihan = listTagihan;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tagihan_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Tagihan tagihan = listTagihan.get(position);
        holder.textViews.get(0).setText(tagihan.getBulan());
        holder.textViews.get(1).setText(tagihan.getTotal());
        holder.textViews.get(2).setText("Rp. "+tagihan.getBayar());
        holder.textViews.get(3).setText(tagihan.getTanggal());
        holder.textViews.get(4).setText(tagihan.getLoket());
    }

    @Override
    public int getItemCount() {
        return listTagihan.size();
    }
}
